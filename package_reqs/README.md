## Installation Requirements
### install_requires.txt
This is the Python Setuptools 'install_requires'.  It contains the names and versions Python packages required by your project.

You can use *pip* to automatically download these packages from PyPi:

```bash
pip install my_project_name
```

You can disable using cached packages by adding parameter `--no-cache-dir`

### requirements files
See article (here)[# https://pip.pypa.io/en/latest/user_guide/#requirements-files]

Requirements files are ideal when you need *repeatable*, *deterministic* installations.  You pin very precise versions and tags, including the semantic patch version (ex.  version 4.10.2)

To install, you pass an *additional parameter* to Pip and Setuptools during installation.

```bash
pip install my_project_name -r my_project_name/package_reqs/requirements_v4_LTS.txt
```

### Uninstall
To uninstall all your projects packages:
```bash
pip uninstall -y -r ./my_project_name/package_reqs/install_requires.txt
```
