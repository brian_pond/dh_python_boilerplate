#!/bin/bash

DEV_SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
REPO_DIR=$(realpath "$DEV_SCRIPTS_DIR/../")

cd "$REPO_DIR"

echo "Removing build, dist, and bench.egg-info directories."
rm -rf build
rm -rf dist
rm -rf bench.egg-info

echo "Removing *.pyc files"
find . -type f -name '*.pyc' -delete

echo "Removing all __pycache__ directories"
find . -type d -name '__pycache__' -delete

echo -e "Script complete.\n"

# Someday might want to write similar cleanup using Python:
: <<'MULTILINE_COMMENT'
import os
import shutil

CLEAN_FILES = './build ./dist ./*.pyc ./*.tgz ./*.egg-info'.split(' ')
_ROOT = os.path.abspath(os.path.dirname(__file__))

for path_spec in CLEAN_FILES:
	# Make paths absolute and relative to this path
	search_path = os.path.join(_ROOT, os.path.relpath(path_spec))
	print(search_path)
	print("Search path: {}".format(search_path))

	for path in [str(p) for p in search_path]:
		if not path.startswith(_ROOT):
			# Die if path in CLEAN_FILES is absolute + outside this directory
			raise ValueError("{} is not a path inside {}".format(path, _ROOT))
		print("Removing {}".format(os.path.relpath(path)))
		shutil.rmtree(path)
MULTILINE_COMMENT
