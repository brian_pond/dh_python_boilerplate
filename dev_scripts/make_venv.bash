#!/bin/bash
#
# This script must be "sourced" (executed in context of current shell)
# One reason is because it activates a Python Virtual environment in the current shell
#
# Developer Note: To exit gracefully (whether the script was executed directly or sourced):
#     return -1 2> /dev/null || exit -1
#

# Number of arguments must be 1 or 2.
if [[ $# -ne 1 && $# -ne 2 ]]; then
    echo -e "\nusage:   source make_venv.bash [path_to_python_binary] --rebuild\n"
    return -1 2> /dev/null || exit -1
fi

if [[ $# -eq 1 && "$1" = "--rebuild" ]]; then
    echo -e "\nusage:   source make_venv.bash [path_to_python_binary] --rebuild\n"
    return -1 2> /dev/null || exit -1
fi

# Second argument can only be --rebuild
if [[ $# -eq 2 && "$2" != "--rebuild" ]]; then
    echo -e "\nusage:   source make_venv.bash [path_to_python_binary] --rebuild\n"
    return -1 2> /dev/null || exit -1
fi

# Set and validate path to Python binary.
PYTHON_PATH=$1
{
    PYTHON_VERSION=$("$PYTHON_PATH" --version)
} || {
    echo -e "\nInvalid argument; pass a complete path to a Python3 binary."
    return -1 2> /dev/null || exit -1
}


export PYTHONDONTWRITEBYTECODE=true  # Disable Python bytecode.

# Find absolute path to this script's directory.
THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Get an absolute path to 'pyvenv.d'
ENVDIR=$(realpath "$THISDIR/../pyvenv.d")

echo -e "\n---Python Virtual Environment Builder---"
echo -e "$PYTHON_VERSION at $(which $PYTHON_PATH)\n"

# If argument '--rebuild' passed, remove existing Python virtual environment.
if [[ $2 = '--rebuild' ]] ; then
	rm -rf ~/.cache/pip
    if [[ "$VIRTUAL_ENV" != '' ]]; then
        deactivate
    fi
    rm -rf $ENVDIR
    echo -e "\u2713 Removed previous Python virtual environment."
fi

# If no Python virtual environment exists, create one.
if [ ! -d "$ENVDIR" ]; then
    bash -c "$python_path -m venv $ENVDIR"
    # Test to verify successful.
    if [ "$?" -ne 0 ]; then
        return -1 2> /dev/null || exit -1
    fi
    echo -e "\u2713 Created new Python Virtual environment ($ENVDIR) ..."
    echo -e "[global]\ndisable-pip-version-check = True" > "$ENVDIR/pip.conf"
fi

# Activate Python virtual environment, if not already active.
if [[ "$VIRTUAL_ENV" == '' ]]; then
  source "$ENVDIR/bin/activate"
  echo -e "\u2713 Activated Python virtual environment."
fi

# Default pip logfile should be in the venv directory
export PIP_LOG_FILE="$VIRTUAL_ENV/pip_log.txt"

# Update to latest version of pip.
echo -e "Upgrading pip ..."
pip install --quiet --upgrade pip
echo -e "Installing additional Python tools ..."
pip install --quiet --upgrade setuptools wheel flake8 flake8-tabs semantic-version
echo -e "\u2713 Script complete.\n"
echo -e "Logs for pip will be written to: $PIP_LOG_FILE\n"

# Python 3.6 does not have importlib.resources
if [[ "$PYTHON_VERSION" == 'Python 3.6'* ]]; then
     echo -e "Detected Python 3.6.x.  Installing PyPI package 'importlib_resources'"
    pip install --quiet --upgrade importlib_resources
fi

# Cleanup Variables
unset ENVDIR
unset THISDIR
unset PYTHON_PATH
unset PYTHON_VERSION

echo -e "To install your project, run:\n"
echo "    pip install -e .\n"
