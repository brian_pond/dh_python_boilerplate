""" some_program/__init__.py """
from semantic_version import Version as SemanticVersion

# Setuptools reads this variable (using regex) during package installation:
__version__ = '1.0.0'


def get_package_version():
	""" Returns semantic version of our Python package. """
	return SemanticVersion(__version__)


if __name__ == "__main__":
	raise Exception("This top-level module should never be run as __main__.")

