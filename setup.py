# https://setuptools.readthedocs.io/en/latest/setuptools.html#developer-s-guide
#
# IMPORTANT: By default, pip forcibly redirects all print(), logging(), and stdout to its own log file.
# This cannot be bypassed, except by calling pip with a verbose parameter (ie. pip install -v)
# Keep this in mind, if you attempt to output text in this setup.py.
#
import ast

try:
    from importlib import resources
except ImportError:
    # Python 3.6 requires a PyPI package
    import importlib_resources as resources

import logging
from os import geteuid as os_geteuid
import pathlib
import platform
import re
from shutil import copyfile
import sys

from setuptools import setup, find_packages
from setuptools.command.install import install
from setuptools.command.develop import develop

# Global Variables
_MIN_PYTHON_VERSION = (3, 6, 0) # this package uses f-strings
_INSTALL_REQUIRES_PATH = 'package_reqs/install_requires.txt'
_VERSION_FILE = 'some_program/__init__.py'

_INSTALL_RESOURCES = False
_RESOURCES = ['some_program.resources']
_CONFIG_TARGET = '/etc/some_program'


def build_resource_dict(target_folder):
	''' Build a dictionary of resources, and the directory they will be written to '''
	resource_dict = {}
	for resource in _RESOURCES:
		resource_tuple = tuple(str(resource).split('.'))
		if len(resource_tuple) < 3:
			resource_dict[resource] = pathlib.Path(target_folder)
		else:
			dirpath = pathlib.Path(target_folder) / resource_tuple[2]
			resource_dict[resource] = dirpath
	return resource_dict


def getPackageVersion():
	''' # Retrieve the Package version from variable '__version__', in /some_package/__init__.py '''
	''' # https://packaging.python.org/guides/single-sourcing-package-version/ '''
	_version_pattern = re.compile(r'__version__\s+=\s+(.*)')
	with open(_VERSION_FILE, 'rb') as f:
		any_match = _version_pattern.search(f.read().decode('utf-8'))
		if any_match:
			package_version = str(ast.literal_eval(any_match.group(1)))
		else:
			raise Exception(f"Unable to find application version in file '{_VERSION_FILE}'")
	print(f"Package Version: {package_version}")
	return package_version


def makeInstallRequires():
	with open(_INSTALL_REQUIRES_PATH) as f:
		return f.read().strip().split('\n')


def makePythonRequires():
	''' Workaround because of this issue: https://github.com/pypa/setuptools/issues/1633 '''
	''' python_requires refers to the version of the -installed- package. Not the version of the build system.'''
	''' I want to validate both, so I wrote this short function. '''
	assert (sys.version_info >= _MIN_PYTHON_VERSION), \
	       "Minimum Python version is {}".format('.'.join(str(v) for v in _MIN_PYTHON_VERSION))
	return (">= " + platform.python_version())


def preInstallSequence(_setupToolsMode):
	# This line is crucial; Logging Levels don't work properly otherwise.
	logging.basicConfig()
	logger = logging.getLogger(_setupToolsMode)
	logger.setLevel(logging.INFO)
	logger.debug('Pre-Install Sequence. Setuptools mode = {}'.format(_setupToolsMode))

	# Do not install package as root
	if os_geteuid() == 0:
		raise Exception("Install should be performed as a non-privileged user")

	# Create some directories that we'll need.

	if not _INSTALL_RESOURCES:
		return

	for dirpath in build_resource_dict(_CONFIG_TARGET).values():
		if dirpath.exists() and not dirpath.is_dir():
			raise Exception("Program requires a resource directory '{}' but found a file there instead.".format(dirpath))
		if dirpath.exists() and dirpath.is_dir():
			continue
		# Directory does not exist.
		logger.info("Attempting to create directory: '{}'".format(dirpath))
		pathlib.Path(dirpath).mkdir(parents=True, exist_ok=True)


def copyResources(_resourcePath, _targetDir, _logger):
	""" This Python 'resources' concept is a headache
	    Loading resources means loading the modules (__init__.py) and running their code.
	"""
	targetDirPath = pathlib.Path(_targetDir)

	# Build a List of resources (file that aren't Python code but we want to copy.)
	_myresources = [rsrc for rsrc in resources.contents(_resourcePath)
	                if (resources.is_resource(_resourcePath, rsrc) is True and rsrc != '__init__.py')]

	for _resource in _myresources:
		with resources.path(_resourcePath, _resource) as srcFilePath:
			if not srcFilePath:
				raise FileNotFoundError("Unable to find absolute path to source {}".format(_resource))
			targetFilePath = pathlib.Path(targetDirPath / _resource)
			_logger.debug("Destination Path = {}".format(targetFilePath))

			# IMPORTANT!  Do -not- overwrite existing /etc/some_package/ files.
			# CLI must offer a way to voluntarily download and override user's configuration"
			if pathlib.Path(targetFilePath).is_file():
				_logger.debug("File \'{}\' already exists; skipping.".format(targetFilePath))
			else:
				copyfile(srcFilePath, targetFilePath)


def postInstallSequence(_setupToolsMode):
	logging.basicConfig()
	logger = logging.getLogger(_setupToolsMode)
	logger.setLevel(logging.INFO)
	logger.debug('Post-Install Sequence. Setuptools mode = {}'.format(_setupToolsMode))
	if _INSTALL_RESOURCES:
		print("Installing resources...")
		for resource, dirpath in build_resource_dict(_CONFIG_TARGET).items():
			copyResources(resource, dirpath, logger)


class InstallOverride(install):
	"""Additional work when running in Install mode."""
	''' https://www.anomaly.net.au/blog/running-pre-and-post-install-jobs-for-your-python-packages/ '''
	def run(self):
		preInstallSequence('install')
		install.run(self)
		postInstallSequence('install')


class DevelopOverride(develop):
	"""Additional work when running in Develop mode."""
	''' https://www.anomaly.net.au/blog/running-pre-and-post-install-jobs-for-your-python-packages/ '''
	def run(self):
		preInstallSequence('develop')
		develop.run(self)
		postInstallSequence('develop')


def get_entry_points():
	# https://amir.rachum.com/blog/2017/07/28/python-entry-points/
	ret = '''
	[console_scripts]
	some_package=some_program.cli:entry_point
	'''
	return ret


# IMPORTANT: Installation should gracefully fail if _CONFIG_TARGET does not exist, or is not writeable.
if not pathlib.Path(_CONFIG_TARGET).is_dir() is False:
    sys.tracebacklimit = 0
    raise FileNotFoundError(f"Configuration directory does not exist: '{_CONFIG_TARGET}'")

setup(
	# TODO: Define a 'provides' keyword
	name='some_package',  # This is the name pip will refer to.
	version=getPackageVersion(),
	packages=find_packages(),
	python_requires=makePythonRequires(),
	install_requires=makeInstallRequires(),
	entry_points=get_entry_points(),
	# handling Data Files and package data
	include_package_data=True,
	# metadata to display on PyPI
	author='Brian Pond',
	author_email='brian@pondconsulting.net',
	description='',
	keywords='',
	license='',
	platforms=['MacOS, Linux'],
	url='',
	zip_safe=False,
	# Additional code to run Before & After installation.
	cmdclass={'install': InstallOverride,
	          'develop': DevelopOverride}
)
