## Python Boilerplate

This project is just some boilerplate for my Python projects.  It includes the following features:

* Bash script `dev_scripts/make_venv.bash` for quickly (re)creating a virtual environment.
* Click command line interface pre-configured in `some_program/cli.py`
* An advanced `setup.py` with support for Pacakge Resources
  * Resources in `some_program/resources` will be written to `/etc/some_program`

### Usage

```
git clone https://gitlab.com/brian_pond/dh_python_boilerplate.git my_project.repo
cd my_project.repo
rm -rf .git  # remove the boilerplate's git
git init     # add your own git
git add .
git commit - m "Initial commit"
```